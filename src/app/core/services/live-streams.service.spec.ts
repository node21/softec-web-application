import { TestBed } from '@angular/core/testing';

import { LiveStreamsService } from './live-streams.service';

describe('LiveStreamsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LiveStreamsService = TestBed.get(LiveStreamsService);
    expect(service).toBeTruthy();
  });
});
