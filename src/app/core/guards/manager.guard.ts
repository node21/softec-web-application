import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AuthService } from '../authentication/auth.service';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';
import { User } from '../models/User';

@Injectable({ providedIn: 'root' })
export class ManagerGuard implements CanActivate {
  constructor(private auth: AuthService, private router: Router) {}

  canActivate(): Observable<boolean> {
    return this.auth.user
      .take(1)
      .map((user: User) => (user.roles.manager === true ? true : false))
      .do(isManager => {
        if (!isManager) {
          console.log('Manager only');
          this.router.navigate(['/login']);
        }
      });
  }
}
