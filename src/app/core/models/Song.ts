export interface Song {
  uid?: string;
  artist?: string;
  genre?: string;
  title?: string;
  paid?: boolean;
  link?: string;
  uploaderId?: string;
  type?: string;
}
