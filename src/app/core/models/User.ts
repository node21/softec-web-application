export interface Roles {
  user?: boolean;
  manager?: boolean;
  singer?: boolean;
  producer?: boolean;
}

export interface User {
  uid?: string;
  email?: string;
  password?: string;
  photoURL?: string;
  displayName?: string;
  roles?: Roles;
  membership?: {
    status?: string;
    plan?: string;
  };
  subscriptionExpiry?: Date;
}
