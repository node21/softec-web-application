interface Member {
  name?: string;
  phone?: string;
  email?: string;
}

export interface Event {
  name?: string;
  uid?: string;
  startTime?: Date;
  endTime?: Date;
  location?: number[];
  managerId?: string;
  members?: Member[];
}
