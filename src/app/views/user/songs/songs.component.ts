import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { pagesToggleService } from '../../../shared/services/toggler.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SongsService } from '../../../core/services/songs.service';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Track } from 'ngx-audio-player';
import { Song } from '../../../core/models/Song';
import { AuthService } from '../../../core/authentication/auth.service';

declare var pg: any;
@Component({
  selector: 'app-songs',
  templateUrl: './songs.component.html',
  styleUrls: ['./songs.component.scss']
})
export class SongsComponent implements OnInit {
  @ViewChild(DatatableComponent) table: DatatableComponent;
  @ViewChild('addNewSongModal') addNewSongModal: ModalDirective;

  user;
  status;
  userType;
  totalSongs: number;
  paidSongs: number;

  statusSort = 'Select Status';

  //Datetable rows
  songsRows: Song[];

  // Chaching our rows. To restore later.
  songSort: Song[];
  isCollapsed = false;

  songForm: FormGroup;

  scrollBarHorizontal = window.innerWidth < 960;
  columnModeSetting = window.innerWidth < 960 ? 'standard' : 'force';
  columnModeSettingSmall = window.innerWidth < 560 ? 'standard' : 'force';
  constructor(
    private songsService: SongsService,
    private fb: FormBuilder,
    private toggler: pagesToggleService,
    private auth: AuthService
  ) {
    window.onresize = () => {
      this.scrollBarHorizontal = window.innerWidth < 960;
      this.columnModeSetting = window.innerWidth < 960 ? 'standard' : 'force';
      this.columnModeSettingSmall =
        window.innerWidth < 560 ? 'standard' : 'force';
    };
    this.toggler.setContent('');
  }

  ngOnInit() {
    this.songsService.getSongs().subscribe(songs => {
      this.songsRows = songs;
      this.songSort = songs;

      this.updateFilterStatus('Free');
    });

    this.auth.user.subscribe(user => {
      this.user = user;
      this.status = this.user ? this.user.membership.status : undefined;
      // if(this.user.membership.status) {
      //   this.status = this.user.membership.status;
      // }
    });

    this.songForm = this.fb.group({
      title: ['', Validators.required],
      link: ['', Validators.required],
      type: ['paid', Validators.required]
    });
    this.msaapPlaylist = [
      {
        title: 'Song 1',
        link: 'https://www.computerhope.com/jargon/m/example.mp3'
      }
    ];

    // this.paidSongs = this.songsRows.filter(x => {
    //   return x.type == 'Paid';
    // }).length;
  }

  get title() {
    return this.songForm.get('title');
  }

  get link() {
    return this.songForm.get('link');
  }

  get type() {
    return this.songForm.get('type');
  }

  handleChange(e) {
    if (e.fileList.length > 1) {
      e.fileList.shift();
    }

    this.songForm.patchValue({
      link: e.file.url
    });
  }

  addSong() {
    this.songForm.get('type').setValue(this.userType);
    console.log(this.songForm.value);
    this.songsService.addSong(this.songForm.value);
    this.addNewSongModal.hide();
  }

  showModal() {
    this.addNewSongModal.show();
  }
  msaapDisplayTitle = true;
  msaapDisplayPlayList = true;
  msaapPageSizeOptions = [3, 5, 7];
  msaapPlaylist: Track[];

  selected = [];
  onSelect({ selected }) {
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
    console.log(selected);
  }

  ngAfterViewInit() {
    this.toggleNavbar();
  }
  setFullScreen() {
    pg.setFullScreen(document.querySelector('html'));
  }

  onActivate(e) {}

  onCheckboxChangeFn($event) {
    console.log($event);
  }

  @HostListener('window:resize', [])
  onResize() {
    this.toggleNavbar();
  }

  toggleNavbar() {
    this.isCollapsed = window.innerWidth < 1025;
  }

  updateFilterSearch(event) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.songSort.filter(function(d) {
      return d.title.toLowerCase().indexOf(val) !== -1 || !val;
    });

    this.songsRows = temp;
    this.table.offset = 0;
  }

  updateFilterStatus(status) {
    if (status == '') {
      this.statusSort = 'Free';
    } else this.statusSort = status;

    console.log(this.statusSort);

    status = status.toLowerCase();
    // filter our data
    const temp = this.songSort.filter(function(d) {
      return d.type.toLowerCase().indexOf(status) !== -1 || !status;
    });

    this.songsRows = temp;
    this.table.offset = 0;
  }

  addToPlaylist() {
    this.msaapPlaylist = this.selected;
  }
}
