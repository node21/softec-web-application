import { Routes } from '@angular/router';

//Componenets
import { SongsComponent } from './songs/songs.component';
import { EventsComponent } from './events/events.component';
import { LiveStreamsComponent } from './live-streams/live-streams.component';
import { AddEventComponent } from './events/add-event/add-event.component';
import { PaymentComponent } from './payment/payment.component';
import { SingleEventComponent } from './events/single-event/single-event.component';

export const AppRoutes: Routes = [
  {
    path: '',
    redirectTo: 'songs',
    pathMatch: 'full'
  },
  {
    path: 'songs',
    component: SongsComponent
  },
  {
    path: 'events',
    component: EventsComponent
  },
  {
    path: 'liveStreams',
    component: LiveStreamsComponent
  },
  {
    path: 'add-event',
    component: AddEventComponent
  },
  {
    path: 'subscriptions',
    component: PaymentComponent
  },
  {
    path: 'single-event/:id',
    component: SingleEventComponent
  }
];
