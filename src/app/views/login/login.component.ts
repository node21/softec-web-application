import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthService } from '../../core/authentication/auth.service';
import { Router } from '@angular/router';
import { UserIdleService } from 'angular-user-idle';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UsersService } from '../../core/services/users.service';
import { pagesToggleService } from '../../shared/services/toggler.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  // userName: string;
  // password: string;

  @ViewChild('addNewUserModal') addNewUserModal: ModalDirective;

  userForm: FormGroup;

  type: string = 'user';

  constructor(
    private auth: AuthService,
    private router: Router,
    private userIdle: UserIdleService,
    private userService: UsersService,
    private fb: FormBuilder,
    private toggler: pagesToggleService
  ) {}

  ngOnInit() {
    // this.userName = '';
    // this.password = '';

    this.userForm = this.fb.group({
      name: ['kamran', Validators.required],
      password: ['123123123', [Validators.required, Validators.minLength(6)]],
      email: ['m4kamran008@gmail.com', [Validators.required, Validators.email]],
      displayPicture: ['', Validators.required]
    });

    //Start watching for user inactivity.
    this.userIdle.startWatching();

    // Start watching when user idle is starting.
    this.userIdle.onTimerStart().subscribe(count => console.log(count));

    // Start watch when time is up.
    this.userIdle.onTimeout().subscribe(() => {
      console.log('Time is up!');
      this.auth.signOut();
    });
  }

  onSubmit() {
    this.auth.signIn(this.email.value, this.password.value).then(() => {
      this.router.navigate['/songs'];
    });
  }

  signUp() {
    this.userService.createUser(this.userForm.value).then(() => {
      this.router.navigate(['/songs']);
    });
    // this.auth.signUp(this.userForm.value)
  }

  facebookLogin() {
    this.auth.facebookSignin();
  }

  googleLogin() {
    this.auth.googleSignin(this.type);
  }

  googleSignup() {
    this.auth.googleSignup();
  }

  get name() {
    return this.userForm.get('name');
  }

  get email() {
    return this.userForm.get('email');
  }

  get password() {
    return this.userForm.get('password');
  }

  addUser() {
    this.userService.createUser(this.userForm.value);
    this.addNewUserModal.hide();
  }

  showModal() {
    this.addNewUserModal.show();
  }

  //Fileupload
  handleChange(e) {
    if (e.fileList.length > 1) {
      e.fileList.shift();
    }

    this.userForm.patchValue({
      displayPicture: e.file.url
    });
  }
}
